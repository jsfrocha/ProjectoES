<div class="navbar navbar-inverse navbar-fixed-top">
	<div class="navbar-inner">
		<div class="container">
			<a class="brand" href="/">Cookbook</a>
			<form method="POST" action="/recipes/search" class="navbar-search pull-right">
				<input type="text" name="param" class="search-query span3" placeholder="Procurar"/>
				<div class="icon-search"></div>
			</form>
		</div>
	</div>
</div>	
