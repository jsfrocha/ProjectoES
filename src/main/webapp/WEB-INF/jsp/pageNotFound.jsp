<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<%@ include file ="/WEB-INF/jsp/common/config.html" %>
	
	<title>Page Not Found</title>
	
</head>
<body>

	<%@ include file ="/WEB-INF/jsp/common/header.jsp" %>
    
	<div class="container">
		<br />
		<br />
		<div class="span12 pagination-centered"><img src="/static/img/page-not-found.jpg" class="img-rounded"></div>
	</div>
	
<%@ include file ="/WEB-INF/jsp/common/scripts.html" %>
	
</body>
</html>