<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<%@ include file ="/WEB-INF/jsp/common/config.html" %>
	
	<title>Detalhe Receita</title>

</head>
<body>
<%@ include file ="/WEB-INF/jsp/common/header.jsp" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
	
<div class="container">
	<div class="well well-large">
		<div class="row">	
 			<div class="span5 pull-right">
				<h4 class="text-center">Versões</h4>
				<table class="table table-bordered table-striped">
					<thead>
						<tr>
							<th>ID</th>
							<th>Autor</th>
							<th>Data</th>
							<th>Reverter</th>
					   	</tr>
					</thead>
					<tbody data-provides="rowlink">
						<c:set var="totalVersions" value="${versionCount}"/>
						<c:forEach var="version" items='${versionList}' varStatus="versionCount">
							<tr>
								<c:choose>
									<c:when test="${versionCount.count > (totalVersions - 1)}">
										<td><a href="#" class="rowlink"></a><a class="btn btn-small btn-success disabled" href="#" role="button" class="btn"><i class="icon-arrow-left icon-white"></i></a></td>
										<td>Versão Actual</td>
										<td>Versão Actual</td>
										<td class="nolink"><a class="btn btn-mini btn-danger disabled" href="#" role="button" class="btn" data-toggle="modal"><i class="icon-remove icon-white"></i> Reverter</a></td>
									</c:when>
									<c:otherwise>
										<td><a href="/versions/${version.oid}" class="rowlink"></a>${versionCount.count}</td>
										<td>${version.VNomeAutor}</td>
										<td>${version.VTimestampFormatted}</td>
										<td class="nolink"><a class="btn btn-mini btn-danger" href="/versions/${version.oid}/revert" role="button" class="btn" data-toggle="modal"><i class="icon-arrow-left icon-white"></i> Reverter</a></td>
									</c:otherwise>
								</c:choose>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div> <%-- Span5 Right End --%>
			
			<div class="span5 pull-left">
				<h3 class="text-center">${recipe.tituloReceita}</h3>
				<br />		

				<h4>Problema</h4>
				<p>${recipe.descProblema}</p>
				<br />

				<h4>Solução</h4>
				<p>${recipe.descSolucao}</p>
				<br />
							
				<h4>Tags</h4>
				<c:forEach var="tag" items="${tagList}">
					<span class="myTag">${tag}</span>
				</c:forEach>
				<br />
				<br />
				<br />
			
				<h5 class="text-center">Receita criada em ${recipe.timestampFormatted} por</h5>
				<p class="text-center">${recipe.nomeAutor}</p>
				<br />			

				<br />
				<br />

			</div><%-- Span5 Left End --%>
		</div> <%-- Row End --%>
		
		
		<a class="btn btn-primary pull-left" href="/recipes"><i class="icon-arrow-left icon-white"></i> Lista</a>
		
		<div class="btn-group">
			<a class="btn btn-warning" href="/recipes/${recipe.oid}/modify"><i class="icon-wrench icon-white"></i> Alterar Receita</a>
			<a class="btn btn-danger" href="#deleteModal" role="button" class="btn" data-toggle="modal"><i class="icon-remove icon-white"></i> Apagar Receita</a></td>
		</div>

		<a class="btn btn-success" href="/recipes/create"><i class="icon-plus icon-white"></i> Nova Receita</a>

	</div> <%-- Well End --%>
</div><%-- Container Master End --%>

<%-- Delete Modal --%>
	<div class="modal hide fade" id="deleteModal">
	  <div class="modal-header">
	    <h3>Apagar Receita</h3>
	  </div>
	  <div class="modal-body" id="modalbody">
	    <p>Tem a certeza que quer apagar a receita <strong>${recipe.tituloReceita}</strong> ?</p>
	  </div>
	  <div class="modal-footer">
	    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="icon-remove icon-white"></i> Não quero!</button>
	    <a href="/recipes/${recipe.oid}/delete" class="btn btn-success"><i class="icon-ok icon-white"></i> Sim, quero!</a>
	  </div>
	</div>
<%-- /Delete Modal --%>

<%@ include file ="/WEB-INF/jsp/common/scripts.html" %>

</body>
</html>