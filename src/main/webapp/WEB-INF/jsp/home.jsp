<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<%@ include file ="/WEB-INF/jsp/common/config.html" %>

	<title>Home - Projecto ES</title>

</head>
<body>

<%@ include file ="/WEB-INF/jsp/common/header.jsp" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>		<%-- Para usar c: ... --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>  <%-- Para usar fn:length --%>

<div class="container main">
	<div class="hero-unit">
		<h1>Cookbook</h1>
		<h3>Projecto de Engenharia de Software</h3>
		<br />

		<c:choose>
			<c:when test="${recipeCount == 1}">
				<p>Existe actualmente ${recipeCount} receita no Cookbook</p>
				<br />
			</c:when>
			<c:otherwise>
				<p>Existem actualmente ${recipeCount} receitas no Cookbook</p>
				<br />
			</c:otherwise>
		</c:choose>
		
		<a class="btn btn-primary" href="/recipes/create"><i class="icon-plus icon-white"></i> Criar Receita</a>
		<a class="btn btn-success" href="/recipes"><i class="icon-list-alt icon-white"></i> Listar Receitas</a>
	</div>
</div>

<%-- Ultimas Receitas --%>
<div class="container receitas">
	<h2>Últimas Receitas</h2>
	<br />
		<div class="hunitcustom">
		<div class="rowcustom">
			<%-- 'Cálculo' Ultimas Receitas --%>
			<c:set var="totalRecipes" value="${recipeCount}"/>
			<c:forEach var="recipe" items='${recipeSortTimeList}' varStatus="recipeCounter">
				<c:if test="${recipeCounter.count > (totalRecipes - 3)}">	
					<a class="span3custom" href="/recipes/${recipe.oid}">			
						<h3>${recipe.tituloReceita}</h3>
						<p>${recipe.timestampFormatted}</p>
						<br />
					</a>
				</c:if>
			</c:forEach>
<%-- /Ultimas Receitas --%>

		</div>
	</div>
	
	<hr> <%-- Linha de Rodapé --%>

	<footer>
		<p class="text-center">Esta página foi gerada às ${currentTime}, no dia ${currentDate}</p>
	</footer>
	
</div>

<%@ include file ="/WEB-INF/jsp/common/scripts.html" %>
	
	
</body>
</html>