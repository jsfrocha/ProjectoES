<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<%@ include file ="/WEB-INF/jsp/common/config.html" %>
	
	<title>Procura</title>

</head>
<body>
<%@ include file ="/WEB-INF/jsp/common/header.jsp" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
	
<div class="container">
	<div class="well well-large">
		<h3 class="text-center">Resultados da Pesquisa por: ${searchParam}</h3>
			<table class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>Título da Receita</th>
						<th>Nome do Autor</th>
						<th>Ultima Modificação</th>
					</tr>
				</thead>
				<tbody data-provides="rowlink">
					<c:forEach var="recipe" items="${recipes}">
						<tr>
							<td><a href="/recipes/${recipe.oid}" class="rowlink">${recipe.tituloReceita}</a></td>
							<td>${recipe.nomeAutor}</td>
							<td>${recipe.timestampFormatted}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
	</div> <%-- Well End --%>
</div><%-- Container Master End --%>

<%@ include file ="/WEB-INF/jsp/common/scripts.html" %>


</body>
</html>