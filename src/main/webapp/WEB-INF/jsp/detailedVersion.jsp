<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<%@ include file ="/WEB-INF/jsp/common/config.html" %>
	
	<title>Detalhe Versão</title>

</head>
<body>
<%@ include file ="/WEB-INF/jsp/common/header.jsp" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
	
<div class="container">
	<div class="well well-large">
		<h3 class="text-center">${version.VTituloReceita}</h3>
		<br />		

		<h4>Problema</h4>
		<p>${version.VDescProblema}</p>
		<br />

		<h4>Solução</h4>
		<p>${version.VDescSolucao}</p>
		<br />
		<br />
		
		<h4>Tags</h4>
		<c:forEach var="tag" items="${tagList}">
			<span class="myTag">${tag}</span>
		</c:forEach>
		<br />
		<br />
			
		<h5 class="text-center">Receita modificada em ${version.VTimestampFormatted} por</h5>
		<p class="text-center">${version.VNomeAutor}</p>
		<br />			

		<br />
		<br />
	
		<div class="btn-group">
			<a class="btn btn-primary pull-left" href="/recipes/${recipe.oid}"><i class="icon-arrow-left icon-white"></i> Voltar</a>
		</div>
		
		<div class="btn-group pull-right">
			<a class="btn btn-danger" href="/versions/${version.oid}/revert"><i class="icon-arrow-up icon-white"></i> Reverter para esta Versão</a>
		</div>

	
	</div> <%-- Well End --%>
</div><%-- Container Master End --%>

<%@ include file ="/WEB-INF/jsp/common/scripts.html" %>

</body>
</html>