<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<%@ include file ="/WEB-INF/jsp/common/config.html" %>

	<title>Alterar</title>

</head>
<body>

<%@ include file ="/WEB-INF/jsp/common/header.jsp" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="container">
	<div class="hero-unit">
		<form class="form-horizontal" method="POST" action="/recipes/${recipe.oid}/modify">
		<legend>Alterar Receita ${recipe.tituloReceita}</legend>

			<div class="control-group">
				<label class="control-label" for="nomeAutor">Autor</label>
				<div class="controls">
					<input name="nomeAutor" autocomplete="off" type="text" id="nomeAutor" value="${recipe.nomeAutor}" />
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" for="descProblema">Problema</label>
				<div class="controls">
					<textarea name="descProblema" autocomplete="off" rows="3" id="descProblema" >${recipe.descProblema}</textarea>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" for="descSolucao">Solução</label>
				<div class="controls">
					<textarea name="descSolucao" autocomplete="off" rows="3" id="descSolucao" >${recipe.descSolucao}</textarea>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" for="tagReceita">Tags</label>
				<div class="controls">
					<input name="tags" autocomplete="off" type="text" id="tagReceita" class="tagManager" >
					<button type="button" id="apagarTagsModify" class="btn btn-danger">Apagar Tags</button>
				</div>
			</div>
		
			<div class="control-group">
				<div class="controls">
					<button type="submit" class="btn btn-warning" id="inputAlterar"><i class="icon-wrench icon-white"></i> Alterar</button>
				</div>
			</div>
		</form>
	</div>
</div>


<%@ include file ="/WEB-INF/jsp/common/scripts.html" %>

    <script type="text/javascript">
      $(".tagManager").tagsManager({
        prefilled: [${recipe.genTags()}],
        preventSubmitOnEnter: true,
        typeahead: true,
        typeaheadAjaxSource: null,
        typeaheadSource: [${tags}],
        blinkBGColor_1: '#FFFF9C',
        blinkBGColor_2: '#CDE69C',
        hiddenTagListName: 'tagReceita'
      });
    </script>
    <script type="text/javascript">
		$("#apagarTagsModify").click( function(){
			$(".tagManager").tagsManager('empty');
		});
    </script>
		
</body>
</html>