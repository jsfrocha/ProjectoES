<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ include file ="/WEB-INF/jsp/common/config.html" %>

<title>Lista Receitas</title>

</head>
<body>

<%@ include file ="/WEB-INF/jsp/common/header.jsp" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="container">
	<div class="well well-large">
		<div class="container">
			<a class="btn btn-primary" href="/recipes/create"><i class="icon-plus icon-white"></i> Criar Receita</a>
		</div>
		<br />
		<br />
		<table class="table table-bordered table-striped">
			<thead>
				<tr>
					<th>Título da Receita</th>
					<th>Nome do Autor</th>
					<th>Ultima Modificação</th>
					<th>Acções</th>
				</tr>
			</thead>
			<tbody data-provides="rowlink">
				<c:forEach var="recipe" items='${recipeSortTitleList}'>
					<tr>
						<td><a href="/recipes/${recipe.oid}" class="rowlink">${recipe.tituloReceita}</a></td>
			   	  		<td>${recipe.nomeAutor}</td>
						<td>${recipe.timestampFormatted}</td>
						<td class="nolink"><a class="btn btn-small btn-danger" href="#${recipe.oid}" role="button" class="btn" data-toggle="modal"><i class="icon-remove icon-white"></i> Apagar</a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		
	</div>
</div>

<%-- Delete Modals para cada recipe -> ID = recipe.oid --%>
<c:forEach var="recipe" items='${recipeSortTitleList}'>
	<div class="modal hide fade" id="${recipe.oid}">
	  <div class="modal-header">
	    <h3>Apagar Receita</h3>
	  </div>
	  <div class="modal-body" id="modalbody">
	    <p>Tem a certeza que quer apagar a receita <strong>${recipe.tituloReceita}</strong> ?</p>
	  </div>
	  <div class="modal-footer">
	    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="icon-remove icon-white"></i> Não quero!</button>
	    <a href="/recipes/${recipe.oid}/delete" class="btn btn-success"><i class="icon-ok icon-white"></i> Sim, quero!</a>
	  </div>
	</div>
</c:forEach>
<%-- /Delete Modals --%>

<%@ include file ="/WEB-INF/jsp/common/scripts.html" %>
	
</body>
</html>