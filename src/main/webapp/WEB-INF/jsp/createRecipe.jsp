<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<%@ include file ="/WEB-INF/jsp/common/config.html" %>

	<title>Criar</title>

</head>
<body>

<%@ include file ="/WEB-INF/jsp/common/header.jsp" %>

<div class="container">
	<div class="hero-unit">
		<form class="form-horizontal" method="POST" action="/recipes">
		<legend>Criar Receita</legend>

			<div class="control-group">
				<label class="control-label" for="nomeAutor">Autor</label>
				<div class="controls">
					<input name="nomeAutor" autocomplete="off" type="text" id="nomeAutor" placeholder="Insira o seu nome.." required/>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" for="tituloReceita">Título</label>
				<div class="controls">
					<input name="tituloReceita" autocomplete="off" type="text" id="tituloReceita" placeholder="Insira o título da receita.." required/>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" for="descProblema">Problema</label>
				<div class="controls">
					<textarea name="descProblema" autocomplete="off" rows="3" id="descProblema" placeholder="Insira a descrição do problema.." required></textarea>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" for="descSolucao">Solução</label>
				<div class="controls">
					<textarea name="descSolucao" autocomplete="off" rows="3" id="descSolucao" placeholder="Insira a descrição da solução.." required></textarea>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" for="tagReceita">Tags</label>
				<div class="controls">
					<input name="tags" autocomplete="off" type="text" placeholder="Ex: tag1,tag2,tag3" class="tagManager" >
					<button type="button" id="apagarTags" class="btn btn-danger">Apagar Tags</button>
				</div>
			</div>
		
			<div class="control-group">
				<div class="controls">
					<button type="submit" class="btn btn-success" id="inputCriar">Criar</button>
				</div>
			</div>
		</form>
	</div>
</div>

<%@ include file ="/WEB-INF/jsp/common/scripts.html" %>

<script type="text/javascript">
  $(".tagManager").tagsManager({
     preventSubmitOnEnter: true,
     typeahead: true,
     typeaheadAjaxSource: null,
     typeaheadSource: [${tags}],
     blinkBGColor_1: '#FFFF9C',
     blinkBGColor_2: '#CDE69C',
     hiddenTagListName: 'tagReceita'
   });
</script>
 
<script type="text/javascript">
 	$("#apagarTags").click( function(){
		$(".tagManager").tagsManager('empty');
    });
</script>


		
</body>
</html>