package pt.ulht.es.cookbook.controller;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import pt.ulht.es.cookbook.domain.CookbookManager;
import pt.ulht.es.cookbook.domain.Recipe;


@Controller
public class HomeController {
  
	@RequestMapping(method=RequestMethod.GET, value="/")
	public String showHome(Model model) {

		DateTime dt = new DateTime();
		DateTimeFormatter fmtTime = DateTimeFormat.forPattern("HH:mm:ss");
		DateTimeFormatter fmtDate = DateTimeFormat.forPattern("dd-MM-yyyy");
		String currentTime = fmtTime.print(dt);
		String currentDate = fmtDate.print(dt);
		List<Recipe> recipeSortTimeList = new ArrayList<Recipe>(CookbookManager.getInstance().getRecipeSet());
		int recipeCount = CookbookManager.getInstance().getRecipeCount();
		Collections.sort(recipeSortTimeList, new Recipe.TimestampComparator());
       	model.addAttribute("recipeCount", recipeCount);
		model.addAttribute("currentTime", currentTime);
		model.addAttribute("currentDate", currentDate);
		model.addAttribute("recipeSortTimeList", recipeSortTimeList);

		return "home";
	}
}

