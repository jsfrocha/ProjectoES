package pt.ulht.es.cookbook.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import pt.ist.fenixframework.pstm.AbstractDomainObject;
import pt.ulht.es.cookbook.domain.*;
import pt.ulht.es.cookbook.util.AutocompleteManager;
import pt.ulht.es.cookbook.util.SearchManager;

@Controller
public class RecipeController {

	//Mapping para Lista de Receitas 
    @RequestMapping(method=RequestMethod.GET, value="/recipes")
    public String listRecipes(Model model) {
    	
    	List<Recipe> recipeSortTitleList = new ArrayList<Recipe>(CookbookManager.getInstance().getRecipeSet());
  		Collections.sort(recipeSortTitleList, new Recipe.TitleComparator());
		model.addAttribute("recipeSortTitleList", recipeSortTitleList);
		model.addAttribute("recipes", CookbookManager.getInstance().getRecipeSet());

        return "listRecipes";
    }
    
    //Mapping para Detalhe de Receita
    @RequestMapping(method=RequestMethod.GET, value="/recipes/{id}")
    public String showDetailedRecipe(@PathVariable("id") String id, Model model) {
    	
    	int versionCount;
		Recipe recipe = AbstractDomainObject.fromExternalId(id);
		List<Version> versionList = new ArrayList<Version>(recipe.getVersionSet());
		Collections.sort(versionList, new Version.VersionTimeComparator());
		String[] tagList = recipe.showTags();
		versionCount = recipe.getVersionCount();
		model.addAttribute("recipe", recipe);
		model.addAttribute("versionList", versionList);
		model.addAttribute("tagList", tagList);
		model.addAttribute("versionCount", versionCount);
		
		return "detailedRecipe";
    }
    
    //Mapping para mostrar Detalhe da Versão
    @RequestMapping(method = RequestMethod.GET, value="/versions/{id}")
    public String showDetailedVersion(@PathVariable("id") String id, Model model) {
 
    	Version version = AbstractDomainObject.fromExternalId(id);
    	Recipe recipe = version.getRecipe();
    	String[] tagList = version.showTags();
    	model.addAttribute("version", version);
    	model.addAttribute("recipe", recipe);
    	model.addAttribute("tagList", tagList);
     	
    	return "detailedVersion";
    }

    //Mapping para mostrar página 'Criar Receita'
    @RequestMapping(method=RequestMethod.GET, value="/recipes/create")
    public String showCreateForm(Model model){
    	
    	if (CookbookManager.getInstance().getRecipeCount() >= 1) {
    		String tags = AutocompleteManager.getAllTags();
			model.addAttribute("tags", tags); //tags -> "tag1","tag2","tag3","tag4"
    	}
    	return "createRecipe";
    }
    
    //Mapping para apagar receita completa
	@RequestMapping(method = RequestMethod.GET, value = "/recipes/{id}/delete")
	public String deleteRecipe(@PathVariable("id") String id) {
		
		Recipe recipe = AbstractDomainObject.fromExternalId(id);
		recipe.delete();
		
		return "redirect:/recipes";
	}
	
	//Mapping para mostrar form de 'Modificar Receita'
	@RequestMapping(method = RequestMethod.GET, value = "/recipes/{id}/modify")
	public String showModifyForm(@PathVariable("id") String id, Model model) {
		
		Recipe recipe = AbstractDomainObject.fromExternalId(id);
		String tags = AutocompleteManager.getAllTags();
		model.addAttribute("recipe", recipe);
		model.addAttribute("tags", tags);
	
		return "modifyRecipe";
	}

	//Mapping para reverter à versão escolhida
	@RequestMapping(method = RequestMethod.GET, value = "/versions/{id}/revert")
	public String revertToVersion(@PathVariable("id") String id, Model model) {
		
		Version version = AbstractDomainObject.fromExternalId(id);
		Recipe recipe = version.getRecipe();
		
		recipe.setNomeAutor(version.getVNomeAutor());
		recipe.setDescProblema(version.getVDescProblema());
		recipe.setDescSolucao(version.getVDescSolucao());
		recipe.setTituloReceita(version.getVTituloReceita());
		recipe.setTagReceita(version.getVTagReceita());
		recipe.setTimestampFormatted(version.getVTimestampFormatted());
		
		Version versionNew = new Version(version.getVNomeAutor(), version.getVTituloReceita(), version.getVDescProblema(), version.getVDescSolucao(), version.getVTagReceita());
		
		recipe.addVersion(versionNew);
		
		return "redirect:/recipes/" + recipe.getExternalId();
	}
	
	//Mapping para modificar Receita - Submit da Form
	@RequestMapping(method = RequestMethod.POST, value = "/recipes/{id}/modify")
	public String modifyRecipe(@PathVariable("id") String id, @RequestParam Map<String, String> params) {
		
		Recipe recipe = AbstractDomainObject.fromExternalId(id);
		
		String nomeAutor = params.get("nomeAutor");
		String descProblema = params.get("descProblema");
		String descSolucao = params.get("descSolucao");
		String tagReceita = params.get("tagReceita");
		String tituloReceita = recipe.getTituloReceita();
		
		Version version = new Version(nomeAutor, tituloReceita, descProblema, descSolucao, tagReceita);
		
		recipe.setNomeAutor(nomeAutor);
		recipe.setDescProblema(descProblema);
		recipe.setDescSolucao(descSolucao);
		recipe.setTagReceita(tagReceita);
		recipe.setTimestampFormatted(version.getVTimestampFormatted());
		
		recipe.addVersion(version);
		
		return "redirect:/recipes/" + recipe.getExternalId();
	}

    //Mapping para criar Receita - Submit da Form
	@RequestMapping(method = RequestMethod.POST, value = "/recipes")
	public String createRecipe(@RequestParam Map<String,String> params) {
		
		String nomeAutor = params.get("nomeAutor");
		String tituloReceita = params.get("tituloReceita");
		String descProblema = params.get("descProblema");
		String descSolucao = params.get("descSolucao");
		String tagReceita = params.get("tagReceita");
		
		Version version = new Version(nomeAutor, tituloReceita, descProblema, descSolucao, tagReceita);
		Recipe recipe = new Recipe(nomeAutor, tituloReceita, descProblema, descSolucao, tagReceita, version.getOid());
		
		recipe.addVersion(version);
		
		return "redirect:/recipes/" + recipe.getExternalId();
	}

	//Mapping para Search - Submit da form
	@RequestMapping(method = RequestMethod.POST, value = "/recipes/search")
	public String searchRecipes(Model model, @RequestParam("param") String query) {
	
		List<Recipe> resultSet = SearchManager.search(query);
		
		model.addAttribute("searchParam", StringUtils.join(query.split(",")," "));
		model.addAttribute("recipes", resultSet);
		
		return "searchResults";
	}
	
	
}