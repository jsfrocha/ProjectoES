package pt.ulht.es.cookbook.util;

import org.apache.commons.lang.StringUtils;

import pt.ulht.es.cookbook.domain.CookbookManager;
import pt.ulht.es.cookbook.domain.Recipe;

public class AutocompleteManager {

	public static String getAllTags() {
		int i = 0;
		String[] tags = new String[CookbookManager.getInstance().getRecipeCount()];
		for (Recipe recipe : CookbookManager.getInstance().getRecipeSet()) {
			tags[i] = recipe.getTagReceita();
			
			System.out.println("Variavel tags[]: "+tags[i]+" Indice i: "+i);
			i++;
		}//tags[] -> [tag1,tag2;tag3,tag4]
		
		String tagsJoin = tags[0];
		for (int j = 1; j < tags.length ; j++) {
			tagsJoin = tagsJoin.concat(","+tags[j]);
			System.out.println("Variavel tagsJoin: "+tagsJoin+" Indice j: "+j);
		}//tagsJoin -> tag1,tag2,tag3,tag4
		
		String[] tagSplit = tagsJoin.split(","); //tagSplit[] -> [tag1,tag2,tag3,tag4]
		for (int k = 0; k < tagSplit.length ; k++){
			tagSplit[k] = "\""+tagSplit[k]+"\"";
			System.out.println("Variavel tagSplit: "+tagSplit[k]+" Indice k: "+k);
		}//tagSplit[] -> ["tag1","tag2","tag3","tag4"]
		
		
		System.out.println("String Final: "+StringUtils.join(tagSplit,","));
		
		return StringUtils.join(tagSplit,",");
	}
	
}
