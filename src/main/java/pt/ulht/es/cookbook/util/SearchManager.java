package pt.ulht.es.cookbook.util;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.ArrayUtils;

import pt.ulht.es.cookbook.domain.*;

public class SearchManager {

	public static List<Recipe> search(String query) {
		
		String[] searchParams = query.split(" ");
		String[] tagSplit = null;
		String[] titSplit = null;
		String[] probSplit = null;
		String[] solSplit = null;
		String[] autorSplit = null;
		
		List<Recipe> resultSet = new ArrayList<Recipe>();
		for (Recipe recipe : CookbookManager.getInstance().getRecipeSet()) {
				tagSplit = recipe.getTagReceita().split(",");
				titSplit = recipe.getTituloReceita().split(" ");
				probSplit = recipe.getDescProblema().split(" ");
				solSplit = recipe.getDescSolucao().split(" ");
				autorSplit = recipe.getNomeAutor().split(" ");
				
				for (int b = 0; b < tagSplit.length; b++) {
					if (ArrayUtils.contains(searchParams, tagSplit[b])) {
						if(!resultSet.contains(recipe)) {
							resultSet.add(recipe);
							break;
						}
					}
				}
				
				for (int c = 0; c < titSplit.length; c++) {
					if (ArrayUtils.contains(searchParams, titSplit[c])){
						if(!resultSet.contains(recipe)) {
							resultSet.add(recipe);
							break;
						}
					}
				}
				
				for (int d = 0; d < probSplit.length; d++ ) {
					if(ArrayUtils.contains(searchParams, probSplit[d])){
						if(!resultSet.contains(recipe)){
							resultSet.add(recipe);
							break;
						}
					}
				}
				
				for (int e = 0; e < solSplit.length; e++ ) {
					if(ArrayUtils.contains(searchParams, solSplit[e])){
						if(!resultSet.contains(recipe)){
							resultSet.add(recipe);
							break;
						}
					}
				}
				
				for (int f = 0; f < autorSplit.length; f++ ) {
					if(ArrayUtils.contains(searchParams, autorSplit[f])) {
						if(!resultSet.contains(recipe)) {
							resultSet.add(recipe);
							break;
						}
					}
				}
			}
		
		return resultSet;
	}
	
}
