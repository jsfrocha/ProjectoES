package pt.ulht.es.cookbook.domain;

import java.util.Comparator;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class Recipe extends Recipe_Base {
    
	//Construtor
	public Recipe(String nomeAutor, String tituloReceita, String descProblema, String descSolucao, String tagReceita, long recipedio) {
		setCookbookManager(CookbookManager.getInstance());
		setNomeAutor(nomeAutor);
		setTituloReceita(tituloReceita);
		setDescProblema(descProblema);
		setDescSolucao(descSolucao);
		setTagReceita(tagReceita);
		setRecipedio(recipedio);
		
		DateTime dt = new DateTime();
		setCreationTimestamp(dt);
		DateTimeFormatter fmt = DateTimeFormat.forPattern("HH:mm:ss, dd-MM-yyyy");
		setTimestampFormatted(fmt.print(dt));
	}
	
	//Adicionar Versão à Receita
	public void addVersion(String vNomeAutor, String vTituloReceita, String vDescProblema, String vDescSolucao, String vTagReceita) {
		addVersion(new Version(vNomeAutor, vTituloReceita, vDescProblema, vDescSolucao, vTagReceita));
	}
	
	//Comparador por Título
	public static class TitleComparator implements Comparator<Recipe> {

		public int compare(Recipe o1, Recipe o2) {
			return o1.getTituloReceita().toLowerCase().compareTo(o2.getTituloReceita().toLowerCase());
		}
	}
	
	//Comparador por Timestamp
	public static class TimestampComparator implements Comparator<Recipe> {
		
		public int compare(Recipe o1, Recipe o2) {
			return o1.getCreationTimestamp().compareTo(o2.getCreationTimestamp());
		}
	}
	
	//Apagar Receita
	public void delete() {
		for (Version version : getVersionSet()) {
			version.delete();
		}
		setCookbookManager(null);
		super.deleteDomainObject();
	}
	
	//Mostrar Tags associadas à Receita - TagsManager
	public String genTags() {

		String[] tagSplit = getTagReceita().split(",");
		
		for (int i = 0; i < tagSplit.length ; i++)
			tagSplit[i] = "\""+tagSplit[i]+"\"";
					
		return StringUtils.join(tagSplit, ",");
	}
	
	//Testes - TagsManager
	public String[] showTags() {
		
		String[] tagSplit = getTagReceita().split(",");
		
		return tagSplit;
	}
    
}
