package pt.ulht.es.cookbook.domain;

import java.util.Comparator;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class Version extends Version_Base {
    
	//Construtor
    public  Version(String vNomeAutor, String vTituloReceita, String vDescProblema, String vDescSolucao, String vTagReceita) {
    	setVNomeAutor(vNomeAutor);
    	setVTituloReceita(vTituloReceita);
    	setVDescProblema(vDescProblema);
    	setVDescSolucao(vDescSolucao);
    	setVTagReceita(vTagReceita);
	
		DateTime dt = new DateTime();
		setVCreationTimestamp(dt);
		DateTimeFormatter fmt = DateTimeFormat.forPattern("HH:mm:ss, dd-MM-yyyy");
		setVTimestampFormatted(fmt.print(dt));
   }
   
    //Comparador por Timestamp
	public static class VersionTimeComparator implements Comparator<Version> {

		public int compare(Version o1, Version o2) {
			return o1.getVCreationTimestamp().compareTo(o2.getVCreationTimestamp());
		}
	}
	
	//Apagar Versão
	public void delete() {
		setRecipe(null);
		super.deleteDomainObject();
	}
	
	//Testes - TagsManager
	public String[] showTags() {
		
		String[] tagSplit = getVTagReceita().split(",");
		
		return tagSplit;
	}
}

