# Projecto de Engenharia de Software

## Cookbook
	
#### Versão 1.0

- Receitas podem ser criadas
- Receitas podem ser listadas
- BugFix: Nome da receita é agora clicável no Firefox

#### Versão 1.1

- Testes:
	- Adicionados nomes e números dos elementos do Grupo ao README
	- Modificado home.jsp e HomeController.java

#### Versão 1.2

- Implementação de Bootstrap
	- Adicionado header.jsp
		- Fixed Navbar
	- Modificação home.jsp
		- Hero-Unit

#### Versão 1.3

- Adicionado ficheiro common/config.html a todas as páginas
	- Para eliminar redundância de configurações nos .jsp

##### Bootstrap

- Adicionado common/header.jsp a todas as páginas
- Adicionado "padding" ao ficheiro common/config.html

##### Navegação

- Caixa "Procurar" na Navbar
- Botão "Criar nova Receita"
- Botão "Listar Receitas"

#### Versão 1.4

##### Bootstrap

- Adicionado createRecipe.jsp
	- Adicionado form para "Criar nova Receita"
- Modificado pageNotFound.jsp
- Modificado recipeNotFound.jsp

#### Versão 1.5

- Layout final
	- Todos os requisitos cumpridos

#### Versão 1.6

##### Checkpoint nº 1 - Requisitos Cumpridos

- R1: Criar Receita
- R2: Listar Receitas
- R3: Ver Receita em Detalhe


#### Versão 1.6.1

##### Mudanças no Layout

- Comentários no Código
- Funcionalidade "Ver Ultimas Receitas"
- Modificações no CSS base do Bootstrap (src/main/webapp/WEB-INF/jsp/common/config.html)

#### Versão 1.6.2

#### Mudanças no Layout

- detailedRecipe.jsp -> Preparado para versionamento de receitas

#### Versão 2.0

- Implementação de persistência - FénixFramework

#### Versão 2.1

##### Checkpoint 2 - Requisitos Cumpridos

- R4: Apagar Receita
- R5: Classificar Receitas
- R8: Persistência (FénixFramework)

#### Versão 3.0

##### Checkpoint 3 - Requisitos Cumpridos

- R6: Procurar Receitas
- R7: Edição de Receitas
